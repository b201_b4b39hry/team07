﻿using UnityEngine;

namespace lucidGame
{
    public class CheckForCraftedBoat : MonoBehaviour
    {
        [SerializeField] private BoatController m_BoatController;
        [SerializeField] private GameObject blockingColliderObject;

        private void Update()
        {
            if (m_BoatController.isBoatCrafted)
            {
                blockingColliderObject.SetActive(false);
            }
        }
    }
}
