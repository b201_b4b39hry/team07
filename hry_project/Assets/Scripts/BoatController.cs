﻿using System;
using System.Collections.Generic;
using lucidGame.Control.Inventory;
using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame
{
    public class BoatController : MonoBehaviour/*, IPointerClickHandler*/
    {
        [SerializeField] private Item requiredInventoryItem;

        [SerializeField] private GameObject platform;

        [SerializeField] private float boatSpeed;
        
        
        [SerializeField] private Transform groundCheck;
        [SerializeField] private float groundCheckRadius;
        [SerializeField] private LayerMask waterLayers;
        [SerializeField] private Transform playerCheck;
        [SerializeField] private float playerCheckRadius;
        [SerializeField] private LayerMask playerLayer;

        [SerializeField] private Transform playerSeat;

        [SerializeField] private GameObject nearbyCheck;
        [SerializeField] private Transform rightShoreCheck;
        [SerializeField] private LayerMask rightShoreLayer;

        [SerializeField] private GameObject arrivalBlockage;
        //[SerializeField] private Vector3 playerPosition;
        
        //ToolTip
        [SerializeField] private GameObject toolTip;
        
        private GameObject m_Player;
        private Inventory m_Inventory;
        private PlayerController m_PlayerController;
        private Animator m_PlayerAnimator;
        private PushPullObjectController m_PushPullObjectController;
        private Transform m_HeadCheck;
        private Transform m_KneeCheck;
        private float m_Direction;
        private Rigidbody2D m_BoatRB;
        

        public bool isBoatCrafted = false;
        public bool isOnWater = false;
        public bool isPlayerOnBoat = false;
        public bool isPlayerAttached = false;
        public bool isPlayerNearby = false;
        public bool boatArrived = false;
        
        // Start is called before the first frame update
        void Start()
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_Inventory = m_Player.GetComponent<Inventory>();
            m_PlayerController = m_Player.GetComponent<PlayerController>();
            m_HeadCheck = m_PlayerController.headLevel;
            m_KneeCheck = m_PlayerController.kneeLevel;
            m_PlayerAnimator = m_Player.GetComponent<Animator>();
            m_PushPullObjectController = GetComponent<PushPullObjectController>();

            m_BoatRB = GetComponent<Rigidbody2D>();
            
            platform.SetActive(false);
            arrivalBlockage.SetActive(false);
            
            toolTip.SetActive(false);

        }

        // Update is called once per frame
        void Update()
        {
            MoveBoat();
        }

        private void FixedUpdate()
        {
            isOnWater = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, waterLayers);
            isPlayerOnBoat = Physics2D.OverlapCircle(playerCheck.position, playerCheckRadius, playerLayer);
            isPlayerNearby = nearbyCheck.GetComponent<PlayerNearbyBoat>().IsPlayerNearby();
            //boatArrived = Physics2D.OverlapCircle(rightShoreCheck.position, 0.2f, rightShoreLayer);
            CheckBoatArrival();
            
            if (boatArrived)
            {
                if (isPlayerAttached)
                {
                    DetachPlayerFromBoat();
                }
                m_PushPullObjectController.FreezeThisObject();
                m_BoatRB.constraints = RigidbodyConstraints2D.FreezeAll;
                arrivalBlockage.SetActive(true);
            }
            else if (isOnWater)
            {
                m_PushPullObjectController.UnFreezeThisObject();
                toolTip.SetActive(false);
            }

            
        }
        public void OnMouseDown()
        {
            //CheckInventoryForItem();
            if (!isBoatCrafted) CraftBoat();
            else if (isBoatCrafted && !isPlayerAttached)
            {
                AttachPlayerToBoat();
            }
            else if (isPlayerAttached)
            {
                DetachPlayerFromBoat();
            }
        }
        


        private void CraftBoat()
        {
            if (isPlayerNearby && CheckInventoryForItem())
            {
                //Set active a sprite
                InteractAnimation();
                isBoatCrafted = true;
                platform.SetActive(isBoatCrafted);
                
                //SetActive toolTip
                toolTip.SetActive(true);
            }
        }

        private void AttachPlayerToBoat()
        {
            if (isPlayerOnBoat && !boatArrived)
            {
                isPlayerAttached = true;
                if (!m_PlayerController.m_FacingRight)
                {
                    //m_PlayerController.m_FacingRight = true;
                    m_PlayerController.Flip(1);
                }
                
                // Play player animation and attach player
                m_PlayerAnimator.Play("Player_Interaction");
                m_PlayerAnimator.SetBool("Interact", true);
                
                // Set the player seat on the boat
                playerSeat.position = m_Player.transform.position;
                //m_Player.transform.parent = gameObject.transform;
            }
        }

        private void DetachPlayerFromBoat()
        {
            isPlayerAttached = false;
            m_PlayerController.isFrozen = false;
            m_PlayerAnimator.SetBool("Interact", false);
            //m_Player.transform.parent = null;
        }

        private void CheckBoatArrival()
        {
            bool colliderCheck  = Physics2D.OverlapCircle(rightShoreCheck.position, 0.2f, rightShoreLayer);
            if (isPlayerAttached && colliderCheck)
            {
                boatArrived = true;
            }
        }

        private void MoveBoat()
        {
            // There is a function to move the boat and the player
            m_Direction = Input.GetAxisRaw("Horizontal");

            if (isOnWater && isPlayerAttached)
            {
                // Move boat and player
                // Animate player
                m_BoatRB.velocity = new Vector2(m_Direction * boatSpeed, m_BoatRB.velocity.y);
                m_Player.transform.position = playerSeat.position;

            }
            
        }
        private bool CheckInventoryForItem()
        {
            bool m_Return = false;
            if (requiredInventoryItem != null)
            {
                //Debug.Log("You need an item: " + requiredInventoryItem);
                //Debug.Log(m_Inventory.slots.Length);
                for (int i = 0; i < m_Inventory.slots.Length; i++)
                {
                    if (m_Inventory.slots[i].transform.childCount != 0)
                    {
                        DragDropItem currentItem = m_Inventory.slots[i].transform.GetChild(0).gameObject
                            .GetComponent<DragDropItem>();
                        //Debug.Log(m_Inventory.slots[i].transform.childCount);
                        //Debug.Log(m_Inventory.slots[i].transform.GetChild(0).gameObject.GetComponent<DragDropItem>());
                        if (currentItem.thisItem == requiredInventoryItem)
                        {
                            Destroy(currentItem.gameObject);
                            m_Inventory.isFull[i] = false;
                            //Debug.Log("Hej");
                            m_Return = true;
                            
                            break;
                        }
                    }
                }
            }
            else m_Return = true;
            return m_Return;
        }
        
        private void InteractAnimation()
        {
            float itemY = transform.position.y;
            float playerHead = m_HeadCheck.position.y;
            float playerKnees = m_KneeCheck.position.y;
            if (isPlayerOnBoat)
            {
                if (itemY < playerHead && itemY > playerKnees)
                {
                    m_PlayerAnimator.Play("Player_PickUpMid");
                }
                else if (itemY > playerHead)
                {
                    m_PlayerAnimator.Play("Player_PickUpTop");
                }
                else if (itemY < playerKnees)
                {
                    m_PlayerAnimator.Play("Player_PickUpBot");
                }
            }
            else
            {
                m_PlayerAnimator.Play("Player_Interaction");
                m_PlayerAnimator.SetBool("Interact", true);
                float interactionTime = m_PlayerController.m_PlayerAnimator.GetCurrentAnimatorStateInfo(0).length;
                Invoke("StopInteracting", interactionTime);
            }
        }

        private void StopInteracting()
        {
            m_PlayerAnimator.SetBool("Interact", false);
        }

    }
}
