﻿using System.Collections;
using UnityEngine;

namespace lucidGame
{
    public class WaitingToGetIntoShaft : MonoBehaviour
    {
        [SerializeField] private bool waitingForShaft;

        [SerializeField] private GameObject pet;
        private PetController m_PetController;
        
        
        [SerializeField] private GameObject m_Player;
        [SerializeField] private Transform ventEnter;

        [SerializeField] private float jumpOnPlatformTime, enterVentTime;

        [SerializeField] private GameObject jumpInVentCutscene;

        [SerializeField] private Transform finishPosition;

        private Vector2 ventPosition;
        private Vector2 platformPosition;

        private void Start()
        {
            waitingForShaft = false;
            m_PetController = pet.GetComponent<PetController>();
            pet = this.gameObject;
            ventPosition = ventEnter.position;
            m_PetController = pet.GetComponent<PetController>();
            jumpInVentCutscene.SetActive(false);
        }


        private void Update()
        {
            if (waitingForShaft)
            {
                //m_PetController.GhostObject();   
                m_PetController.isFrozen = true;
            }
        }


        public void EnterShaft()
        {
            

            float offset = 0.3f;
            if (transform.position.x <= ventEnter.position.x)
            {
                offset *= -1;
            }
            
            platformPosition = new Vector2(m_Player.transform.position.x, 
                m_Player.GetComponent<Collider2D>().bounds.min.y + pet.GetComponent<Collider2D>().bounds.extents.y/* + pet.GetComponent<Collider2D>().offset.y*/);
            
            //platformPosition = new Vector2(ventEnter.transform.position.x + offset, 
            //    m_Player.GetComponent<Collider2D>().bounds.min.y + pet.GetComponent<Collider2D>().bounds.extents.y/* + pet.GetComponent<Collider2D>().offset.y*/);
            
            // ledge.GetComponent<Collider2D>().bounds.max.y + m_PlayerController.col.bounds.extents.y - m_PlayerController.col.offset.y

            StartCoroutine(JumpOnPlatform(platformPosition, jumpOnPlatformTime));
            
            Invoke("JumpInVent", jumpOnPlatformTime + 0.3f);
        }
        
        IEnumerator JumpOnPlatform(Vector2 goalPosition, float durat)
        {
            float time = 0;

            m_PetController.isJumping = true;
            m_PetController.isIdling = false;
            Vector2 startValue = pet.transform.position;

            while (time < durat)
            {
                pet.transform.position = Vector2.Lerp(startValue, goalPosition, time / durat);
                time += Time.deltaTime;
                yield return null;
            }

            m_PetController.isJumping = false;
            m_PetController.isIdling = true;
            
        }
        
        
        private void JumpInVent()
        {
            StartCoroutine(JumpInVentCor(ventPosition, enterVentTime));
        }
        
        IEnumerator JumpInVentCor(Vector2 goalPosition, float durat)
        {
            Flip();
            float time = 0;

            m_PetController.isJumping = true;
            m_PetController.isIdling = false;
            Vector2 startValue = pet.transform.position;

            while (time < durat)
            {
                pet.transform.position = Vector2.Lerp(startValue, goalPosition, time / durat);
                time += Time.deltaTime;
                yield return null;
            }

            m_PetController.isJumping = false;
            m_PetController.isIdling = true;
            
            LookLeft();
            jumpInVentCutscene.SetActive(true);
            transform.position = finishPosition.position;
            Color tmp = GetComponent<SpriteRenderer>().color;
            tmp.a = 0f;
            GetComponent<SpriteRenderer>().color = tmp;
            Invoke("RevealPet", 8.5f);
        }

        private void RevealPet()
        {
            Color tmp = GetComponent<SpriteRenderer>().color;
            tmp.a = 255f;
            GetComponent<SpriteRenderer>().color = tmp;
        }

        private void Flip()
        {
            float _direction = ventPosition.x - transform.position.x;
            if (_direction < 0 && m_PetController.isFacingRight)
            {
                Vector3 flippedScale = transform.localScale;
                flippedScale.x *= -1;
                transform.localScale = flippedScale;
            }
            else if (_direction > 0 && !m_PetController.isFacingRight)
            {
                Vector3 flippedScale = transform.localScale;
                flippedScale.x *= -1;
                transform.localScale = flippedScale;
            }
        }

        private void LookLeft()
        {
            if (m_PetController.isFacingRight)
            {
                Vector3 flippedScale = transform.localScale;
                flippedScale.x *= -1;
                transform.localScale = flippedScale;
            }
        }

        
        public bool WaitingForShaft
        {
            get => waitingForShaft;
            set => waitingForShaft = value;
        }
    }
}
