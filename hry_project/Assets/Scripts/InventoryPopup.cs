using UnityEngine;
using UnityEngine.EventSystems;

namespace lucidGame
{
    public class InventoryPopup : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {

        [SerializeField] private GameObject inventoryPanel;
        //public float animationSpeed;
        //private float animationTime;
        [SerializeField] public bool showInventory;
        //private bool showInventory;
        // Start is called before the first frame update
        void Start()
        {
            //inventoryPanel.SetActive(false);
            showInventory = false;
        }
        
        void Update()
        {
            if (showInventory)
            {
                this.transform.GetComponent<Animator>().Play("InventoryPopup");
            }
            else if (!showInventory)
            {
                this.transform.GetComponent<Animator>().Play("InventoryHide");
            }
        }
        /*
        public void OnMouseOver()
        {
            inventoryPanel.SetActive(true);
            Debug.Log("You entered a collider");
        }

        public void OnMouseExit()
        {
            inventoryPanel.SetActive(false);
            Debug.Log("You exited a collider");
        }
        */

        public void PopAndHide()
        {
            showInventory = true;
            Invoke("HideInventory", 1.0f);
        }

        private void HideInventory()
        {
            showInventory = false;
        }
        public void OnPointerEnter(PointerEventData eventData)
        { 
            showInventory = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            showInventory = false;
        }
        
        public bool ShowInv
        {
            get {
                return showInventory;
            }
            set
            {
                showInventory = value;
            }
        }
        
    }
}
