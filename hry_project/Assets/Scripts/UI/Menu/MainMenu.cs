﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace lucidGame.UI.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public void StartGame(){
            //SceneManager.LoadScene("Level_1_HP");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        public void ContinueGame(){
            Application.Quit();
        }

        public void ExitGame(){
            Debug.Log("Exit");
            Application.Quit();
        }
    }
}
