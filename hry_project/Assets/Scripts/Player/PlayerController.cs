﻿using System;
using System.Collections;
using UnityEngine;

namespace lucidGame
{
    public class PlayerController : MonoBehaviour
    {
        [NonSerialized] public Rigidbody2D rb;
        [NonSerialized] public Collider2D col;

        [SerializeField] public float m_DefaultMoveSpeed;
        [SerializeField] private float lightOnSpeed;
        [SerializeField] private float m_DragSpeed;


        [NonSerialized] public Animator m_PlayerAnimator;
        [NonSerialized] private float moveSpeed;


        // Checkers
        [SerializeField] public LayerMask groundLayers;
        [SerializeField] protected Transform groundCheckCollider;
        [SerializeField] protected Transform frontCheckCollider;
        [SerializeField] protected Transform backCheckCollider;

        [SerializeField] public Transform headLevel;
        [SerializeField] public Transform kneeLevel;

        private AudioSource footstep;

        [SerializeField] public Transform castPoint; // Eye position
        [SerializeField] public Transform frontReachCheck;

        // Constants

        protected const float groundCheckRadius = 0.2f;
        [NonSerialized] public float frontReach = 0.2f;


        // Player States
        public bool m_FacingRight;
        public bool isFrozen;
        public bool isGrounded;
        public bool isTouchingFront;
        public bool isTouchingBack;
        public bool isLightOn;
        public bool isDragging;
        public bool canDrag;
        public bool isReachingFront;
        public bool isClimbing;
        public bool canClimb;

        // Climbing Variables
        private PlayerClimbController m_ClimbController;
        private float m_Direction;

        private float m_ForwardDirection;

        void Start()
        {
            m_ClimbController = GetComponent<PlayerClimbController>();
            m_PlayerAnimator = GetComponent<Animator>();
            col = GetComponent<Collider2D>();
            rb = GetComponent<Rigidbody2D>();
            moveSpeed = m_DefaultMoveSpeed;
            m_FacingRight = true;
            footstep = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            // Do not change player scale
            transform.rotation = transform.rotation;
            transform.localScale = transform.localScale;
            LightOn();
            m_ClimbController.CheckLedge();
            //Climb();
        }

        private void FixedUpdate()
        {
            // Check for ground
            isGrounded = Physics2D.OverlapCircle(groundCheckCollider.position, groundCheckRadius, groundLayers);
            isTouchingFront = Physics2D.OverlapCircle(frontCheckCollider.position, 0.1f, groundLayers);
            isTouchingBack = Physics2D.OverlapCircle(backCheckCollider.position, 0.1f, groundLayers);
            m_Direction = Input.GetAxisRaw("Horizontal");
            //isReachingFront = Physics2D.OverlapCircle(frontReachCheck.position, groundCheckRadius, groundLayers);


            m_ClimbController.LedgeClimb();
            Flip(m_Direction);
            if ((m_FacingRight && m_Direction > 0) || (!m_FacingRight && m_Direction < 0)) m_ForwardDirection = 1;
            else if ((!m_FacingRight && m_Direction > 0) || (m_FacingRight && m_Direction < 0))
                m_ForwardDirection = (-1);
            else if (m_Direction == 0) m_ForwardDirection = 0;
            //Debug.Log(m_ForwardDirection);
            if (isGrounded && !isFrozen/* && !isTouchingFront && !isDragging*/)
            {
                //LightOn();
                if (isLightOn)
                {
                    setMoveSpeed(lightOnSpeed);

                }

                else if (!isDragging && !isReachingFront)
                {
                    setMoveSpeed(m_DefaultMoveSpeed);
                }

                else if (!isDragging && /*isReachingFront*/ isTouchingFront)
                {
                    // Do not move and put your hand against the wall
                    setMoveSpeed(0);
                }

                if ((!isTouchingFront && !isTouchingBack) || (isDragging && !isTouchingBack))
                {
                    m_PlayerAnimator.SetFloat("Walk", Mathf.Abs(m_Direction));
                    m_PlayerAnimator.SetFloat("Direction", m_ForwardDirection);
                    //Debug.Log("Touching in front");
                }

                else
                {
                    m_PlayerAnimator.SetFloat("Direction", 0);
                    m_PlayerAnimator.SetFloat("Walk", 0);
                }

                //m_PlayerAnimator.SetFloat("Walk", Mathf.Abs(m_Direction)); 
                rb.velocity = new Vector2(m_Direction * moveSpeed, rb.velocity.y);
            }

            /*
            else
            {
                //m_PlayerAnimator.SetFloat("Direction", 0);
                m_PlayerAnimator.SetFloat("Walk", 0); 
            }
            */
            
            FreezePlayer();
        }

        private void LightOn()
        {
            if (Input.GetButtonDown("LightUp") && !isDragging)
            {
                if (isLightOn)
                {
                    isLightOn = false;
                }
                else
                {
                    isLightOn = true;
                }

                m_PlayerAnimator.SetBool("LightOn", isLightOn);
            }

        }


        private void FreezePlayer()
        {
            if (isFrozen)
            {
                rb.velocity = Vector2.zero;
                rb.bodyType = RigidbodyType2D.Kinematic;
                m_Direction = 0;
            }
            else
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
            }
        }

        public void Flip(float moveSideway)
        {
            if (!isDragging && !isFrozen)
            {
                if (moveSideway > 0 && !m_FacingRight || moveSideway < 0 && m_FacingRight)
                {
                    m_FacingRight = !m_FacingRight;
                
                    Vector3 theScale = transform.localScale;

                    theScale.x *= -1;
                
                    transform.localScale = theScale;
                }
            }
        }
        

        public float DragSpeed
        {
            get{
                return this.m_DragSpeed;
            }
        }
        public void setMoveSpeed(float newSpeed)
        {
            moveSpeed = newSpeed;
        }

        private void FootstepSound(){
            footstep.Play();
        }

    }
    
}
