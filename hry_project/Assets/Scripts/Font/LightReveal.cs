﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightReveal : MonoBehaviour
{

    public bool m_IsNearby;
    public bool getNearbyLight;

    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Hints"))
        {
            Debug.Log("Hint is nearby");
            m_IsNearby = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Hints"))
        {
            Debug.Log("Left Hint's range");
            m_IsNearby = false;
        }
    }

    public bool getNearby()
    {
        getNearbyLight = m_IsNearby;
        return getNearbyLight;
    }


}
