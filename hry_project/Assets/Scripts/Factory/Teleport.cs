﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{

    public GameObject m_Door;
    [SerializeField] private GameObject m_Player;

    private bool m_IsNearby;
    private Animator m_PlayerAnimator;
    
    // Start is called before the first frame update
    void Start()
    {
        m_IsNearby = false;
        m_Player = GameObject.FindGameObjectWithTag("Player");
        m_PlayerAnimator = m_Player.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            m_IsNearby = true;
        }
    }
    
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            m_IsNearby = false;
        }
    }
    
    private void OnMouseDown()
    {
        if (m_IsNearby)
        {
            m_PlayerAnimator.Play("Player_PickUpMid");
            StartCoroutine(TeleportFun());
        }
    }

    IEnumerator TeleportFun()
    {
        yield return new WaitForSeconds(1);
        m_Player.transform.position = new Vector2(m_Door.transform.position.x, m_Door.transform.position.y);
    }
}
