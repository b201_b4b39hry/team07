﻿using System.Collections;
using System.Collections.Generic;
using lucidGame.Control.Inventory;
using UnityEngine;

public class CraftableItem : MonoBehaviour
{

    public Item item;

    public string titleName;
    public string description;

    public string requirement;
    public string result;

    public string condition;
    public string interact;
    // Start is called before the first frame update
    
    void Start()
    {
        //item = GetComponent<Item>();
        titleName = item.name;
        description = item.description;
        /*
        requirement = item.requiredItemName;
        result = item.newItemName;

        condition = item.pickUpCondition;
        interact = item.worldInteraction;
        */
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
