﻿using UnityEngine;

namespace lucidGame
{
    public class LightRevealCollider : MonoBehaviour
    {
        private LightReveal parentLightReveal;

        private void Start()
        {
            parentLightReveal = gameObject.transform.parent.GetComponent<LightReveal>();
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Hints"))
            {
                Debug.Log("Hint is nearby");
                parentLightReveal.m_IsNearby = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Hints"))
            {
                Debug.Log("Left Hint's range");
                parentLightReveal.m_IsNearby = false;
            }
        }
    }
}
