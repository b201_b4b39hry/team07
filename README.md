# Team 07

We are introducing a new exciting single player puzzle adventure.

You can find more information on [WIKI](https://gitlab.fel.cvut.cz/b201_b4b39hry/team07/-/wikis/home).


## Team members

* Lucie Rosenthalerová <rosenluc@fel.cvut.cz> - team leader
* Vilém Jonák <jonakvil@fel.cvut.cz>
* Phạm Hoài An <phamhoai@fel.cvut.cz>
* Hung Pham <phamnhun@fel.cvut.cz>