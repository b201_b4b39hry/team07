﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class ExecutionArea : MonoBehaviour
    {
        [SerializeField] private KillYourSelf killController;

        private void OnTriggerEnter2D(Collider2D other)
        {
            killController.isPlayerInArea = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            killController.isPlayerInArea = false;
        }
    }
}
