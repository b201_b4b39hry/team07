﻿using System;
using UnityEngine;

namespace lucidGame
{
    public class RevealGraffiti : MonoBehaviour
    {
        [SerializeField] private GameObject graffiti;
        [SerializeField] private bool showGraffiti;

        [SerializeField] private bool isPlayerNearby;

        [SerializeField] private GameObject gun;

        private Animator m_GraffitiAnimator;
        
        
        void Start()
        {
            showGraffiti = false;
            m_GraffitiAnimator = graffiti.GetComponent<Animator>();
            Color tmp = graffiti.GetComponent<SpriteRenderer>().color;
            tmp.a = 0.0f;
            
            graffiti.GetComponent<SpriteRenderer>().color = tmp;
            
            gun.SetActive(false);
            
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                isPlayerNearby = true;
            }
        }

        private void OnMouseDown()
        {
            if (isPlayerNearby)
            {
                gun.SetActive(true);
                m_GraffitiAnimator.SetBool("RevealGraffiti", true);
            }
        }
    }
}
