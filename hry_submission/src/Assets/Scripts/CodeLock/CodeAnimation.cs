﻿using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;


namespace lucidGame.CodeLock
{
    public class CodeAnimation : MonoBehaviour
    {
        public bool m_IsSafeOpened;
        
        
        private Sprite[] m_Sprite;
        [SerializeField] private GameObject First_1, Second_1, First_2, Second_2, First_3, Second_3, First_4, Second_4, First_5, Second_5;
        [SerializeField] private Button passChar_1, passChar_2, passChar_3, passChar_4, passChar_5;
        [SerializeField] private GameObject safe, body;
        private int passCount_1 = 0, passCount_2 = 0, passCount_3 = 0, passCount_4 = 0, passCount_5 = 0;
        private int[] result, correctCombination;

        Image m_Image;

        void Awake()
        {
            m_Sprite = Resources.LoadAll<Sprite>("Puzzles/Numbers");
        }

        // Start is called before the first frame update
        void Start()
        {
            result = new int[] { 1, 1, 1, 1, 1 };
            correctCombination = new int[] { 3, 5, 2, 7, 6 };
            m_IsSafeOpened = false;
        }
        public void passCharClicked_1()
        {
        
            First_1.GetComponent<Animator>().Play("FirstNumAnim");
            Second_1.GetComponent<Animator>().Play("SecondNumAnim");

            passCount_1++;
            if (passCount_1 == m_Sprite.Length)
            {
                passCount_1 = 0;
            }

            First_1.GetComponent<Image>().sprite = m_Sprite[passCount_1];
            Second_1.GetComponent<Image>().sprite = m_Sprite[passCount_1];
            result[0] = passCount_1 + 1;
            CheckForResult();
        }

        public void passCharClicked_2()
        {
            First_2.GetComponent<Animator>().Play("FirstNumAnim");
            Second_2.GetComponent<Animator>().Play("SecondNumAnim");
            passCount_2++;
            if (passCount_2 == m_Sprite.Length)
            {
                passCount_2 = 0;
            }
            First_2.GetComponent<Image>().sprite = m_Sprite[passCount_2];
            Second_2.GetComponent<Image>().sprite = m_Sprite[passCount_2];
            result[1] = passCount_2 + 1;
            CheckForResult();
        }

        public void passCharClicked_3()
        {
            First_3.GetComponent<Animator>().Play("FirstNumAnim");
            Second_3.GetComponent<Animator>().Play("SecondNumAnim");
            passCount_3++;
            if (passCount_3 == m_Sprite.Length)
            {
                passCount_3 = 0;
            }
            First_3.GetComponent<Image>().sprite = m_Sprite[passCount_3];
            Second_3.GetComponent<Image>().sprite = m_Sprite[passCount_3];
            result[2] = passCount_3 + 1;
            CheckForResult();
        }

        public void passCharClicked_4()
        {
            First_4.GetComponent<Animator>().Play("FirstNumAnim");
            Second_4.GetComponent<Animator>().Play("SecondNumAnim");
            passCount_4++;
            if (passCount_4 == m_Sprite.Length)
            {
                passCount_4 = 0;
            }
            First_4.GetComponent<Image>().sprite = m_Sprite[passCount_4];
            Second_4.GetComponent<Image>().sprite = m_Sprite[passCount_4];
            result[3] = passCount_4 + 1;
            CheckForResult();
        }

        public void passCharClicked_5()
        {
            First_5.GetComponent<Animator>().Play("FirstNumAnim");
            Second_5.GetComponent<Animator>().Play("SecondNumAnim");
            passCount_5++;
            if (passCount_5 == m_Sprite.Length)
            {
                passCount_5 = 0;
            }
            First_5.GetComponent<Image>().sprite = m_Sprite[passCount_5];
            Second_5.GetComponent<Image>().sprite = m_Sprite[passCount_5];
            result[4] = passCount_5 + 1;
            CheckForResult();
        }

        private void CheckForResult()
        {
            if (result[0] == correctCombination[0] && result[1] == correctCombination[1] &&
                result[2] == correctCombination[2]
                && result[3] == correctCombination[3] && result[4] == correctCombination[4])
            {
                Debug.Log("Opened");
                m_IsSafeOpened = true;
                safe.GetComponent<Animator>().Play("OpenLockAnim");
                body.GetComponent<Animator>().Play("OpenBodyAnim");
            }
        }
    }
}
