﻿using UnityEngine;

namespace lucidGame
{
    public class CallLoadScene : MonoBehaviour
    {
        [SerializeField] private SceneLoader sceneLoader;


        public void CallSceneLoader()
        {
            sceneLoader.LoadNextScene();
        }
    }
}
