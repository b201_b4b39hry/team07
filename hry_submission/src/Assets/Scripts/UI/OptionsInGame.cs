﻿using UnityEngine;
using UnityEngine.Audio;

namespace lucidGame
{
    public class OptionsInGame : MonoBehaviour
    {
        public AudioMixer audioMixer;
        private bool isFullscreen;

        void Start(){
            isFullscreen = false;
        }

        void Update(){
            FixedUpdate();
        }

        private void FixedUpdate(){
            if(Input.GetButtonDown("Fullscreen")){
                isFullscreen = !isFullscreen;
                Debug.Log("fullscreen");
                Screen.fullScreen = isFullscreen;
            }
        }

        public void SetVolume (float volume){
            audioMixer.SetFloat("volume", volume);
        }

        public void SetQuality(int key){
            Debug.Log("Quality");
            QualitySettings.SetQualityLevel(key);
        }
    }
}
